# -*- coding: utf-8 -*-
from flask import jsonify
from flask import request


def ok_json(data=''):
    return jsonify(ok=True, data=data)

def fail_json(reason='', http_code=200):
    if http_code==200:
        return jsonify(ok=False, reason=reason)
    else:
        response = Response()
        response.status_code = http_code
        response.data = reason
        return response


def ok_jsonp(data={}, callback_name='callback'):
    jsonp_callback =  request.args.get(callback_name, None)
    if jsonp_callback:
        return Response(
                "%s(%s);" % (jsonp_callback, json.dumps({'ok': True, 'data':data}, ensure_ascii=False)),
                mimetype="text/javascript"
                )
    return ok_json(data)

def fail_jsonp(reason='', callback_name='callback'):
    jsonp_callback =  request.args.get(callback_name, None)
    if jsonp_callback:
        return Response(
                "%s(%s);" % (jsonp_callback, json.dumps({'ok': False, 'reason':reason})),
                mimetype="text/javascript"
                )
    return fail_json(reason)

def get_args(request, *descriptions):
    args = request.get_json() if request.method == 'POST' else request.args
    if not args and request.data and request.method == 'POST':
        args = json.loads(request.data)
    if not args:
        args = request.form
    return args

