#coding: utf-8
import json
import hashlib
import datetime

from flask import Flask, request, jsonify, Response, render_template

from werkzeug import secure_filename
from urlparse import urljoin

from sqlalchemy.orm import relationship, backref, sessionmaker, scoped_session, aliased, mapper
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.sql import select
from flask.ext.admin.contrib.fileadmin import FileAdmin
from flask import Flask
from flask.ext.admin import Admin, BaseView, expose
from flask.ext.admin.contrib.sqla import ModelView

from models import Course,engine
from base import ok_json,fail_json,ok_jsonp,fail_jsonp,get_args
import os
import os.path as op

from ebssdk import course as course_sdk



Session = sessionmaker(bind=engine)
session = Session() 

app = Flask(__name__)
app.secret_key = 'youdontknowme'
admin=Admin(app)

app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024 * 1024


# 设置允许上传的文件类型
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg',  'mp4'])

def allowed_file(filename):
# 判断文件的扩展名是否在配置项ALLOWED_EXTENSIONS中
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS



# 设置上传文件存放的目录
UPLOAD_FOLDER = './static'
path = op.join(op.dirname(__file__), 'static')
admin.add_view(FileAdmin(path, '/static/', name='Static Files'))

 
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # 获取上传过来的文件对象
        file = request.files['file']
        course_img = request.files['course_img']
        course_title = request.form.get('course_title','default value')
        course_teacher = request.form.get('course_teacher','default value')
        course_info = request.form.get('course_info','default value')
        course_price = request.form.get('course_price','default value')
        upload_time = datetime.datetime.now()
        course_length = request.form.get('course_length','default_value')
        play_times = request.form.get('play_times','defult_value')


# 检查文件对象是否存在，且文件名合法
        if course_img and allowed_file(course_img.filename):
            course_img = secure_filename(course_img.filename)
            file.save(os.path.join(UPLOAD_FOLDER, course_img))

        if file and allowed_file(file.filename):
            # 去除文件名中不合法的内容
            filename = secure_filename(file.filename)
            # 将文件保存在本地UPLOAD_FOLDER目录下
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            
            video_upload = Course(video_url=filename,course_img=course_img, course_title=course_title,course_teacher=course_teacher,
                                 course_info=course_info,course_price=course_price,upload_time=upload_time,course_length=course_length,
                                 play_times=play_times)          
#            print video_upload
            session.add(video_upload)

            try:
                session.commit()
            except:
                session.rollback()

            return 'Upload successful!!!'


        else:    # 文件不合法
            return 'Upload Failed'
    else:    # GET方法
        return render_template('upload.html')


@app.route('/create_course',methods=['GET','POST'])
def create_course():
    if request.method == 'POST':
        user_id = request.form.get('user_id')
        title = request.form.get('title')
        introduction = request.form.get('introducation')
    
        course = course_sdk.core_create_course(user_id, introduction, title)
   
        if course['ok']:
        #return 'create course successful!'
            return ok_jsonp(course['data'])
        else:
            return fail_jsonp('create course faild!')
    else:
        return render_template('create_course.html')





@app.route('/api/course/query',methods=['GET','POST'])
def course_query():
    args = get_args(request)
    course_id = args.get('course_id')
    upload = session.query(Course).filter_by(id=course_id).first()

    if upload is not None:
        url1 = 'http://127.0.0.1:8000/static/'
        url2 = upload.video_url
        filepath = urljoin(url1,url2)

        a = 'http://127.0.0.1:8000/static/'
        b = upload.course_img
        imgpath = urljoin(a,b)

        return ok_jsonp({"id":upload.id,"course_img":imgpath,"video_url":filepath,"course_title":upload.course_title,
                        "course_info":upload.course_info,"course_teacher":upload.course_teacher,"course_price":upload.course_price,
                        "course_length":upload.course_length,"upload_tiome":upload.upload_time,"play_times":upload.play_times})
    
    else:
        return fail_jsonp("sorry,id erroe")


@app.route('/api/course/list',methods=['GET','POST']) 
def course_list():
    get_course = session.query(Course).all()
    course_list = [e.to_json() for e in get_course]
   
    return ok_jsonp(data=course_list)



if __name__ == '__main__':    
    app.run(debug=True, host='0.0.0.0', port=8000)

