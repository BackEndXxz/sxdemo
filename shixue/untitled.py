#coding: utf-8
__author__      = "swsend"


import json
import hashlib
import datetime

from flask import Flask, request, jsonify, Response, render_template

from werkzeug import secure_filename
from urlparse import urljoin

from sqlalchemy.orm import relationship, backref, sessionmaker, scoped_session, aliased, mapper
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.sql import select
from flask import Flask

from models import Course,engine
from base import ok_json,fail_json,ok_jsonp,fail_jsonp,get_args
import os
import os.path as op

from ebssdk import course as sdk_course
from ebssdk import video as skd_video


app = Flask(__name__)
app.secret_key = 'youdontknowme'



# search



@app.route('/api/course/search/info',methods=['GET'])
def z_seach_course():
    id_list = []
    s = request.args.get('s', None)
    if not s:
        abort(400) 
    id_list_resp =  sdk_course.core_get_course_id_list_by_search(s)

    id_list = id_list_resp['data']
      
    course_query = sdk_course.core_get_courses_by_ids(id_list)["data"]

    print type(id_list)


    if id_list_resp['ok']:
        return ok_json(course_query)
    return fail_json(id_list_resp['reason'])


if __name__ == '__main__':    
    app.run() 
