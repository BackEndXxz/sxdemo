#coding=utf-8
__author__='shshi'

import requests_wrapper as rest 

# Query
def core_get_assignment_by_id(id):
    return rest.get('/core/api/assignment/query/info/', locals(), ('id', ))

def core_get_assignment_id_list_by_course(course_id):
    return rest.get('/core/api/assignment/query/id/list/by/course/', locals(), ('course_id', ))

def core_get_assignment_id_list_by_courses(course_ids, order_by=None, assignto_id=None, filter='all'):
    ids_str = course_ids
    if type(ids_str) is not str:
        ids_str = ','.join(course_ids)
    course_ids = ids_str
    return rest.get('/core/api/assignment/query/id/list/by/courses/', locals(), ('course_ids', 'order_by', 'assignto_id', 'filter'))

def core_get_assignments_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/assignment/query/batch/', locals(), ('ids', ))

def core_get_submission_id_list_by_assignment(assignment_id, user_id=None, order_by=None, score=None):
    return rest.get('/core/api/assignment/query/submission/id/list/', locals(), ('assignment_id', 'user_id', 'order_by', 'score'))

def core_get_submission_id_list_by_user(user_id):
    return rest.get('/core/api/assignment/query/submission/id/list/by/user/', locals(), ('user_id', ))

def core_get_submission(id):
    return rest.get('/core/api/assignment/query/submission/', locals(), ('id', ))

def core_get_submissions_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/assignment/query/submissions/', locals(), ('ids', ))

def core_get_submission_page_id_list(submission_id):
    return rest.get('/core/api/assignment/query/submission/page/id/list/', locals(), ('submission_id', ))

def core_get_submission_pages_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/assignment/query/submission/pages/', locals(), ('ids', ))

# Create
def core_create_assignment(course_id, user_id, content, deadline, full_score=100.0, allow_submit=True, title='untitled', id=None):
    url = '/core/api/assignment/create/'
    return rest.post(url, locals(), ('course_id', 'user_id', 'content', 'deadline', 'full_score', 'allow_submit', 'title', 'id'))

def core_insert_attach_file(assignment_id, file_id, title):
    url = '/core/api/assignment/insert/attached/'
    return rest.post(url, locals(), ('assignment_id', 'file_id', 'title'))

def core_insert_submission(assignment_id, file_id, user_id, custom_title='untitled', score=None, id=None):
    url = '/core/api/assignment/insert/submission/'
    return rest.post(url, locals(), ('assignment_id', 'file_id', 'user_id', 'custom_title', 'score', 'id'))

def core_insert_submission_page(submission_id, file_id, file_location, id=None):
    url = '/core/api/assignment/insert/submission/page/'
    return rest.post(url, locals(), ('submission_id', 'file_id', 'file_location', 'id'))

# Update
def core_update_assignment(id, content=None, deadline=None, full_score=None, allow_submit=None, title=None):
    url = '/core/api/assignment/update/'
    return rest.post(url, locals(), ('id', 'content', 'deadline', 'full_score', 'allow_submit', 'title'))

def core_update_submission(id, score=None, file_id=None, custom_title=None):
    url = '/core/api/assignment/update/submission/'
    return rest.post(url, locals(), ('id', 'file_id', 'custom_title', 'score'))

# Delete
def core_delete_assignment_attached(id):
    url = '/core/api/assignment/delete/attached/'
    return rest.delete(url, id)

def core_delete_assignment(id):
    url = '/core/api/assignment/delete/'
    return rest.delete(url, id)

def core_delete_submission_page(id):
    url = '/core/api/assignment/delete/submission/page/'
    return rest.delete(url, id)

def core_batch_delete_submission_pages(ids):
    url = '/core/api/assignment/delete/submission/pages/'
    if type(ids) is not str:
        ids_str = ','.join(ids)
        ids = ids_str
    return rest.post(url, locals(), ('ids', ))
