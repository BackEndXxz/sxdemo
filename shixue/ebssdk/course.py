#coding=utf-8
__author__='shshi'

import requests_wrapper as rest 

# Query

# course info
def core_get_course_by_id(id, require_password=None):
    return rest.get('/core/api/course/query/info/basic/', locals(), ('id', 'require_password'))

def core_get_all_course_id_list():
    return rest.get('/core/api/course/query/all/id/list/', locals(), ())

def core_get_course_id_list_by_search(s):
    return rest.get('/core/api/course/query/search/id/list/', locals(), ('s', ))

def core_get_course_id_list_by_arrangement_time(start_at, end_at):
    return rest.get('/core/api/course/query/id/list/by/arranment/time/', locals(), ('start_at', 'end_at'))

def core_get_courses_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/by/ids/', locals(), ('ids', ))

def core_get_course_addition_by_id(course_id):
    return rest.get('/core/api/course/query/info/addition/', locals(), ('course_id', ))

# user related course
def core_get_course_id_list_by_user_id(user_id):
    return rest.get('/core/api/course/query/id/list/by/user/', locals(), ('user_id', ))

def core_get_attend_record_id_list_by_user_and_course_id(user_id, course_id, attend_type=None):
    return rest.get('/core/api/course/query/user/attend/record/id/list/', locals(), ('user_id', 'course_id', 'attend_type'))

def core_get_attend_records_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/user/attend/records/', locals(), ('ids', ))

# arrangement
def core_get_arrangement_by_id(id):
    return rest.get('/core/api/course/query/arrangement/info/', locals(), ('id', ))

def core_get_arrangement_ids(course_id):
    return rest.get('/core/api/course/query/arrangement/id/list/', locals(), ('course_id', ))

def core_get_arrangements_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/arrangements/', locals(), ('ids', ))

# cooperation
def core_get_all_cooperation_school_ids():
    return rest.get('/core/api/course/query/all/cooperation/school/id/list/', locals(), ())

def core_get_all_cooperation_school_ids_with_count():
    return rest.get('/core/api/course/query/all/cooperation/school/id/list/with/count/', locals(), ())

def core_get_cooperation_school_ids(course_id):
    return rest.get('/core/api/course/query/cooperation/school/id/list/', locals(), ('course_id', ))

def core_get_course_id_list_by_school_id(school_id):
    return rest.get('/core/api/course/query/id/list/by/school/', locals(), ('school_id', ))

# discipline category
def core_get_discipline_by_id(id):
    return rest.get('/core/api/course/query/discipline/info/', locals(), ('id', ))

def core_get_discipline_category_ids(course_id):
    return rest.get('/core/api/course/query/discipline/category/id/list/', locals(), ('course_id', ))

def core_get_disciplines_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/disciplines/', locals(), ('ids', ))

def core_get_category_by_id(id):
    return rest.get('/core/api/course/query/category/info/', locals(), ('id', ))

def core_get_categorys_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/categorys/', locals(), ('ids'))

def core_get_categorys_by_discipline_id(discipline_id):
    return rest.get('/core/api/course/query/categorys/by/discipline/id/', locals(), ('discipline_id', ))

# reference
def core_get_reference_ids(course_id):
    return rest.get('/core/api/course/query/reference/id/list/', locals(), ('course_id', ))


# section
def core_get_section_by_id(id):
    return rest.get('/core/api/course/query/section/info/', locals(), ('id', ))

def core_get_section_ids(course_id, asc=None):
    return rest.get('/core/api/course/query/section/id/list/', locals(), ('course_id', 'asc'))

def core_get_sections_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/sections/', locals(), ('ids', ))


# frame
def core_get_frame_by_id(id):
    return rest.get('/core/api/course/query/frame/info/', locals(), ('id', ))

def core_get_frame_ids(section_id):
    return rest.get('/core/api/course/query/frame/id/list/', locals(), ('section_id', ))

def core_get_frames_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/frames/', locals(), ('ids', ))


# Section Video

def core_get_section_video_ids(section_id):
    return rest.get('/core/api/course/query/section/video/id/list/', locals(), ('section_id', ))

def core_get_section_videos_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/section/videos/', locals(), ('ids', ))

def core_get_user_visited_course_ids(user_id):
    return rest.get('/core/api/course/query/user/visited/id/list/', locals(), ('user_id', ))

def core_get_user_visited_courses_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/user/visiteds/', locals(), ('ids', ))

def core_get_user_marked_course_ids(user_id):
    return rest.get('/core/api/course/query/user/marked/id/list/', locals(), ('user_id', ))

def core_check_user_marked_course(user_id, course_id):
    return rest.get('/core/api/course/check/user/marked/', locals(), ('user_id', 'course_id'))

def core_check_user_apply_course(user_id, course_id):
    return rest.get('/core/api/course/check/user/apply/', locals(), ('user_id', 'course_id'))

def core_get_course_ids_by_marked(user_id):
    return rest.get('/core/api/course/query/id/list/by/user/marked/', locals(), ('user_id', ))

def core_get_user_marked_courses_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/user/markeds/', locals(), ('ids', ))

def core_get_user_learning_course_ids(user_id):
    return rest.get('/core/api/course/query/user/learning/id/list/', locals(), ('user_id', ))

def core_check_user_learning_course(user_id, course_id):
    return rest.get('/core/api/course/check/user/learning/', locals(), ('user_id', 'course_id'))

def core_get_course_ids_by_learning(user_id):
    return rest.get('/core/api/course/query/id/list/by/user/learning/', locals(), ('user_id', ))

def core_get_user_ids_by_learning(course_id, within_delete=True):
    return rest.get('/core/api/course/query/user/id/list/by/learning/', locals(), ('course_id', within_delete))

def core_get_apply_user_ids_by_course(course_id, status):
    return rest.get('/core/api/course/query/apply/user/id/list/', locals(), ('course_id', 'status'))

def core_get_user_learning_courses_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/user/learnings/', locals(), ('ids', ))

def core_get_voice_domain_by_id(id):
    return rest.get('/core/api/course/query/voice/domain/', locals(), ('id', ))

def core_get_section_replay_ids_by_section(section_id):
    return rest.get('/core/api/course/query/section/replay/id/list/by/section/', locals(), ('section_id', ))

def core_get_section_replay_by_id(id):
    return rest.get('/core/api/course/query/section/replay/', locals(), ('id', ))

def core_get_section_replays_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/course/query/section/replays/by/ids/', locals(), ('ids', ))


# Create

def core_create_course(user_id, introduction, title='untitled', is_private=False, password='', cover_url='', cover_lock=False, id=None):
    url = '/core/api/course/create/'
    return rest.post(url, locals(), ('user_id', 'title', 'introduction', 'is_private', 'password','cover_url', 'cover_lock', 'id'))

def core_create_course_addition_info(course_id, class_time_desc, class_language_id):
    url = '/core/api/course/create/addition/'
    return rest.post(url, locals(), ('course_id', 'class_time_desc', 'class_language_id'))


# arrangement
def core_insert_course_arrangement(course_id, title, start_time, end_time, pre_arrangement_id=None):
    url = '/core/api/course/insert/arrangement/'
    return rest.post(url, locals(), ('course_id', 'pre_arrangement_id', 'title', 'start_time', 'end_time'))


# reference
def core_create_course_reference(course_id, reference_id): 
    url = '/core/api/course/create/reference/'
    return rest.post(url, locals(), ('course_id', 'reference_id'))


# cooperation
def core_create_course_cooperation(course_id, school_id): 
    return rest.post('/core/api/course/create/cooperation/',
            locals(), ('course_id', 'school_id'))

# discipline & category
def core_insert_discipline_category(course_id, discipline_id, category_id=None):
    url = '/core/api/course/insert/discipline/category/'
    return rest.post(url, locals(), ('course_id', 'discipline_id', 'category_id'))

# section
def core_insert_section(course_id, title, introduction=None, export_uri=None, pre_section_id=None, id=None): 
    url = '/core/api/course/insert/section/'
    return rest.post(url, locals(), ('course_id', 'title', 'introduction', 'export_uri', 'pre_section_id', 'id'))

def core_insert_section_video(section_id, video_id, customized_name=None): 
    url = '/core/api/course/insert/section/video/'
    return rest.post(url, locals(), ('section_id', 'video_id', 'customized_name'))


# frame
def core_insert_frame(section_id, title, introduction=None, pre_frame_id=None, id=None): 
    url = '/core/api/course/insert/frame/'
    return rest.post(url, locals(), ('section_id', 'title', 'introduction', 'pre_frame_id', 'id'))


# user related courses
def core_insert_user_visited_course(user_id, course_id): 
    url = '/core/api/course/insert/user/visited/'
    return rest.post(url, locals(), ('course_id', 'user_id'))

def core_insert_user_marked_course(user_id, course_id): 
    url = '/core/api/course/insert/user/marked/'
    return rest.post(url, locals(), ('course_id', 'user_id'))

def core_insert_user_learning_course(user_id, course_id): 
    url = '/core/api/course/insert/user/learning/'
    return rest.post(url, locals(), ('course_id', 'user_id'))

def core_insert_user_apply_course(user_id, course_id): 
    url = '/core/api/course/insert/user/apply/'
    return rest.post(url, locals(), ('course_id', 'user_id'))

def core_insert_user_attend_record(user_id, course_id, attend_type='enter'): 
    url = '/core/api/course/insert/user/attend/record/'
    return rest.post(url, locals(), ('course_id', 'user_id', 'attend_type'))


# section replay
def core_create_section_replay(title, user_id, section_id, audio_location=None, duration=None, id=None):
    url = '/core/api/course/create/section/replay/'
    return rest.post(url, locals(), ('title', 'user_id', 'section_id', 'audio_location', 'duration', 'id'))

# Update
def core_update_course(id, title=None, introduction=None, is_private=None, password=None, cover_url=None, cover_lock=None):
    url = '/core/api/course/update/info/'
    return rest.post(url, locals(), ('id', 'title', 'introduction', 'is_private', 'password', 'cover_url', 'cover_lock'))

def core_update_course_addition(course, class_language=None, class_time_desc=None):
    url = '/core/api/course/update/addition/'
    return rest.get(url, locals(), ('course', 'class_language', 'class_time_desc'))

def core_update_course_arrangement(id, title=None, start_time=None, end_time=None):
    url = '/core/api/course/update/arrangement/'
    return rest.post(url, locals(), ('id', 'title', 'start_time', 'end_time'))

def core_update_section(id, title=None, introduction=None, export_uri=None):
    url = '/core/api/course/update/section/'
    return rest.post(url, locals(), ('id', 'title', 'introduction', 'export_uri'))

def core_update_section_order(course_id, section_id, index):
    url = '/core/api/course/update/section/order/'
    parent_id = course_id
    target_id = section_id
    return rest.post(url, locals(), ('parent_id', 'target_id', 'index'))

def core_update_section_video(id, customized_name=None):
    url = '/core/api/course/update/section/video/'
    return rest.post(url, locals(), ('id', 'customized_name'))

def core_update_frame(id, title=None, introduction=None):
    url = '/core/api/course/update/frame/'
    return rest.post(url, locals(), ('id', 'title', 'introduction'))

def core_update_section_replay(id, title=None, user_id=None, section_id=None, audio_location=None, duration=None):
    url = '/core/api/course/update/section/replay/'
    return rest.post(url, locals(), ('id', 'title', 'user_id', 'section_id', 'audio_location', 'duration'))

def core_update_user_apply_course(id, status):
    url = '/core/api/course/update/user/apply/course/'
    return rest.post(url, locals(), ('id', 'status'))

# Delete


def core_delete_course(id):
    url = '/core/api/course/delete/'
    return rest.delete(url, id)

def core_delete_user_marked_course(id):
    url = '/core/api/course/delete/user/marked/'
    return rest.delete(url, id)

def core_delete_user_learning_course(id):
    url = '/core/api/course/delete/user/learning/'
    return rest.delete(url, id)

def core_delete_course_arrangement(id):
    url = '/core/api/course/delete/arrangement/'
    return rest.delete(url, id)

def core_delete_course_cooperation(id):
    url = '/core/api/course/delete/cooperation/'
    return rest.delete(url, id)

def core_delete_course_reference(id):
    url = '/core/api/course/delete/reference/'
    return rest.delete(url, id)

def core_delete_course_discipline_category(id):
    url = '/core/api/course/delete/discipline/category/'
    return rest.delete(url, id)

def core_delete_section(id):
    url = '/core/api/course/delete/section/'
    return rest.delete(url, id)

def core_delete_frame(id):
    url = '/core/api/course/delete/frame/'
    return rest.delete(url, id)

def core_delete_section_video(id):
    url = '/core/api/course/delete/section/video/'
    return rest.delete(url, id)

def core_delete_section_videos(ids):
    url = '/core/api/course/delete/section/videos/'
    return rest.post(url, locals(), ('ids', )) 

def core_delete_section_replays(ids):
    url = '/core/api/course/batch/delete/section/replay/'
    return rest.post(url, locals(), ('ids', )) 
