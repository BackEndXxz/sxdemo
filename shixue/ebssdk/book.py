#coding=utf-8
__author__='veggie'

import sdk_setting as settings
import requests_wrapper as rest

# Query
def core_get_book_by_id(id):
    return rest.get( '/core/api/book/query/info/', locals(), ('id',))

def core_get_books_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join([str(id) for id in ids])
    ids = ids_str
    return rest.get( '/core/api/books/query/by/ids/', locals(), ('ids',))

# Create
def core_create_book(title, isbn=None, description=None, download_url=None):
    return rest.post( '/core/api/book/create/', locals(),  ('title', 'isbn', 'description', 'download_url'))

# Update
def core_update_book(id, title, isbn, description, download_url):
    return rest.post( '/core/api/book/update/', locals(),  ('id', 'title', 'isbn', 'description', 'download_url'))

# Delete
def core_delete_book(ids):
    return rest.post( '/core/api/book/delete/', locals(), ('ids',))
