#coding=utf-8
__author__='shshi'

import requests_wrapper as rest 

# Query

def core_get_banner_school_id_list():
    return rest.get('/core/api/recommendation/query/banner/school/id/list/', locals(), ())

def core_get_banner_schools_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/recommendation/query/banner/schools/', locals(), ('ids', ))

def core_get_user_may_interested_course_id_list(user_id):
    return rest.get('/core/api/recommendation/query/user/may/interested/course/id/list/', locals(), ('user_id', ))

def core_get_excellent_course_id_list():
    return rest.get('/core/api/recommendation/query/excellent/course/id/list/', locals(), ())
