#coding=utf-8
__author__='shshi'

import requests_wrapper as rest 

# Query
def core_get_school_by_id(id):
    return rest.get('/core/api/school/query/info/', locals(), ('id', ))

def core_get_schools_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/school/query/batch/', locals(), ('ids', ))

def core_search_school(title):
    return rest.get('/core/api/school/search/', locals(), ('title', ))

def core_get_school_by_host(host):
    return rest.get('/core/api/school/query/by/host/', locals(), ('host', ))


