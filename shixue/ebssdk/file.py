#coding=utf-8
__author__='shshi'

import requests_wrapper as rest 

# Query
def core_get_file(id):
    return rest.get('/core/api/file/query/info/', locals(), ('id', ))

def core_get_file_by_hash(hash):
    return rest.get('/core/api/file/query/info/by/hash/', locals(), ('hash', ))

def core_get_files_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join([str(id) for id in ids])
    ids = ids_str
    return rest.get('/core/api/file/query/batch/', locals(), ('ids', ))

def core_get_file_to_images(file_id):
    return rest.get('/core/api/file/query/to/image/', locals(), ('file_id', ))

def core_download_file(location):
    return rest.get('/core/api/file/download/', locals(), ('location', ))

# Create
def core_create_file(filename, content):
    url = '/core/api/file/create/'
    return rest.post_file(url, locals(), (), filename, content)

def core_create_file_to_image(file_id, image_file_id, order):
    url = '/core/api/file/create/to/image/'
    return rest.get(url, locals(), ('file_id', 'image_file_id', 'order'))

# Update
def core_update_file(id, cs=None, type=None):
    return rest.get('/core/api/file/update/', locals(), ('id', 'cs', 'type'))

# Delete
def core_delete_file(id):
    url = '/core/api/file/delete/'
    return rest.delete(url, id)

