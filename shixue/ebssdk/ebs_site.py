#coding=utf-8
__author__='shshi'

import requests_wrapper as rest 

# Query
def core_get_available_configuration():
    return rest.get('/core/api/site/query/available/configuration/', locals(), ())

def core_get_banner_configurations():
    return rest.get('/core/api/site/query/banners/', locals(), ())

