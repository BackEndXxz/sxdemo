#coding=utf-8
__author__='slide'

import requests_wrapper as rest 

# Query

# course info
def core_get_notice_by_id(id):
    return rest.get('/core/api/notice/query/info/', locals(), ('id', ))

def core_batch_get_notice(ids):
    if isinstance(ids, list):
        ids = ','.join([str(x) for x in ids])
    return rest.get('/core/api/notice/query/batch/', locals(), ('ids', ))

def core_notice_list_ids_by_user(user_id, course_ids):
    return rest.get('/core/api/notice/query/list/by/user/', locals(), ('user_id', 'course_ids'))

def core_notice_list_ids_by_type(user_id, notice_type, course_ids=""):
    if isinstance(course_ids, list):
        course_ids = ','.join([str(x) for x in course_ids])
    return rest.get('/core/api/notice/query/list/by/type/', locals(), ("user_id", "notice_type", "course_ids"))

def core_sent_course_notice_list(user_id):
    return rest.get('/core/api/notice/query/sent/course/list/', locals(), ("user_id",))

def core_notice_count(user_id, course_ids="", current_role=""):
    if isinstance(course_ids, list):
        course_ids = ','.join([str(x) for x in course_ids])
    return rest.get('/core/api/notice/query/notice/count/', locals(), ("user_id", "course_ids", "current_role"))

# Create
def core_create_notice(title, content, create_by, notice_to, course_id, type):
    url = '/core/api/notice/create/'
    return rest.post(url, locals(), ('title', 'content', 'create_by', 'notice_to', 'course_id', 'type'))


def core_system_to_user_notice(title, content, notice_to):
    url = '/core/api/notice/create/'
    create_by = 0
    course_id = None
    type = 'system'
    return rest.post(url, locals(), ('title', 'content', 'create_by', 'notice_to', 'course_id', 'type'))





