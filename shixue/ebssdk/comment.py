#coding=utf-8
__author__='shshi'

import requests_wrapper as rest 

# Query
def core_get_comment_by_id(id):
    return rest.get('/core/api/comment/query/info/', locals(), ('id', ))

def core_get_comment_id_list_by_target(target, target_id):
    return rest.get('/core/api/comment/query/id/list/by/target/', locals(), ('target', 'target_id'))

def core_get_course_comment_id_list(course_id):
    target = "Course"
    target_id = course_id
    return core_get_comment_id_list_by_target(target, target_id)

def core_get_comments_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/comment/query/batch/', locals(), ('ids', ))

# Create
def core_create_comment(target, target_id, title, content, user_id):
    url = '/core/api/comment/create/'
    return rest.post(url, locals(), ('target', 'target_id', 'title', 'content', 'user_id'))

# Delete
def core_delete_comment(id):
    url = '/core/api/comment/delete/'
    return rest.delete(url, id)
