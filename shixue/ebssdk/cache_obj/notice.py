#coding=utf-8

import ebssdk.sdk_setting as settings

from ebssdk.cache_obj import cache_rclient
from ebssdk import notice as sdk_notice
from ebssdk import course as sdk_course

class UserNoticeReadCache(object):

    USER_NOTICE_READ_CACHE_PREFIX = 'UNRC'
    USER_UNREAD_NOTICE_COUNT_CACHE_PREFIX = 'UUNC'
    
    @classmethod
    def add_user_read_notice(cls, user_id, notice_id):
        cache_rclient.sadd('%s:%s' % (cls.USER_NOTICE_READ_CACHE_PREFIX, user_id), notice_id)

    @classmethod
    def check_user_notice_is_read(cls, user_id, notice_id): 
        return cache_rclient.sismember('%s:%s' % (cls.USER_NOTICE_READ_CACHE_PREFIX, user_id), notice_id)

    @classmethod
    def get_user_read_notice_ids(cls, user_id): 
        return cache_rclient.smembers('%s:%s' % (cls.USER_NOTICE_READ_CACHE_PREFIX, user_id))

    @classmethod
    def set_user_unread_count(cls, user_id, count, timeout=settings.CACHE_USER_UNREAD_TIMEOUT): 
        cache_rclient.setex('%s:%s' % (cls.USER_UNREAD_NOTICE_COUNT_CACHE_PREFIX, user_id), count, timeout)

    @classmethod
    def get_user_unread_count(cls, user_id): 
        return cache_rclient.get('%s:%s' % (cls.USER_UNREAD_NOTICE_COUNT_CACHE_PREFIX, user_id))

    @classmethod
    def delete_user_unread_count(cls, user_id): 
        return cache_rclient.delete('%s:%s' % (cls.USER_UNREAD_NOTICE_COUNT_CACHE_PREFIX, user_id))

def user_unread_notice_count(user_id):
    count = UserNoticeReadCache.get_user_unread_count(user_id)
    if count:
        return int(count)

    to = user_id
    resp = sdk_course.core_get_course_ids_by_learning(user_id)
    course_ids = []
    if resp['ok']:
        course_ids = resp['data']
    resp = sdk_notice.core_notice_list_ids_by_user(user_id, ','.join(course_ids))
    if resp["ok"]:
        notice_ids = resp["data"]['notice_ids']

    read_notice_set = UserNoticeReadCache.get_user_read_notice_ids(user_id)
    all_notice_set = set([str(x) for x in notice_ids])
    count = len(all_notice_set - read_notice_set)
    # write count cache
    read_notice_set = UserNoticeReadCache.set_user_unread_count(user_id, count)
    return int(count)

def wrap_notice(notice, read_ids):
    if str(notice['id']) in read_ids:
        notice['unread'] = False
    else:
        notice['unread'] = True
    if notice['type'] == 'course':
        resp = sdk_course.core_get_course_by_id(notice['course_id'])
        if resp['ok']:
            notice['course'] = resp['data']
    return notice

def batch_wrap_notice(notice_list, read_ids):
    return [wrap_notice(notice, read_ids) for notice in notice_list]


if __name__ == '__main__':
    resp = sdk_notice.core_notice_list_ids_by_type(12, 'system')
    id_list = resp['data']['notice_ids']
    resp = sdk_notice.core_batch_get_notice(id_list)
    read_ids = UserNoticeReadCache.get_user_read_notice_ids('12')
    for notice in resp['data']:
        print notice
    print 'ss'
    print read_ids
    print batch_wrap_notice(resp['data'], read_ids)

