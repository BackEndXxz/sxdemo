#coding=utf-8

import simplejson
import ebssdk.sdk_setting as settings

from ebssdk import course as sdk_course 

from ebssdk.cache_obj import cache_rclient
from ebssdk.cache_obj.cacheobject import CacheObject, CacheAdapter

class CourseSetting(object):

    _COURSE_SETTING = 'COSE'
    _SECTION_SETTING = 'SESE'

    @classmethod
    def set_current_section(cls, course_id, section_id):
        cache_rclient.hset("%s" % cls._COURSE_SETTING, course_id, section_id)

    @classmethod
    def get_current_section(cls, course_id):
        return cache_rclient.hget("%s" % cls._COURSE_SETTING, course_id)

    @classmethod
    def delete_current_section(cls, course_id):
        return cache_rclient.hdel("%s" % cls._SECTION_SETTING, course_id)

    @classmethod
    def delete_current_frame(cls, section_id):
        return cache_rclient.hdel("%s" % cls._SECTION_SETTING, section_id)

    @classmethod
    def set_current_frame(cls, section_id, frame_id):
        cache_rclient.hset("%s" % cls._SECTION_SETTING, section_id, frame_id)

    @classmethod
    def get_current_frame(cls, section_id):
        return cache_rclient.hget("%s" % cls._SECTION_SETTING, section_id)


def get_current_section(course_id):
    return CourseSetting.get_current_section(course_id)

def set_current_section(course_id, section_id):
    return CourseSetting.set_current_section(course_id, section_id)

def delete_current_section(course_id):
    return CourseSetting.delete_current_section(course_id)

def delete_current_frame(section_id):
    return CourseSetting.delete_current_frame(section_id)

def get_current_frame(section_id):
    return CourseSetting.get_current_frame(section_id)

def set_current_frame(section_id, frame_id):
    return CourseSetting.set_current_frame(section_id, frame_id)


class Course(CacheObject):

    def __init__(self):
        CacheObject.__init__(self)
        self._adapter = CacheAdapter(cache_rclient, 'Course')
        self._timeout = settings.DEFAULT_CACHE_USER_TIMEOUT

    def fetch_data(self, course_id):
        resp = sdk_course.core_get_course_by_id(course_id)
        if resp['ok']:
            course_dic = resp['data']
            self._fetch_data(course_dic)

    def is_active(self):
        return True

    def get_id(self):
        return unicode(self.id)

    def to_json(self):
        return simplejson.dumps(self.get_doc())

def get_course(course_id):
    assert course_id is not None
    course = Course()
    course.fetch_cache(course_id)
    if course.is_dummy():
        course.fetch_data(course_id)
        if not course.is_dummy():
            course.save()
    return course 

def delete_course_cache(course_id):
    course = get_course(course_id)
    course.delete()


if __name__ == '__main__':
    set_current_frame('95', '395') 
    get_current_frame('95') 
