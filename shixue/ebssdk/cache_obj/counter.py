#coding=utf-

from ebssdk.cache_obj import cache_rclient


class ClassroomPopularityCounter(object):
    """
    The Popularity Counter of Classroom
    """

    CLASSROOM_POPULARITY_PREFIX = 'CLMP'

    def __init__(self, classroom_id):
        self.key = '%s:%s' % (self.CLASSROOM_POPULARITY_PREFIX, classroom_id)


    def get_count(self):
        value = cache_rclient.get(self.key)
        if value is None:
            return 0
        else:
            return int(value)

    def set_count(self, value):
        cache_rclient.set(self.key, '%s'%value)

    def delete_count(self):
        cache_rclient.delete(self.key)

    count = property(get_count, set_count, delete_count)

    def increment(self, incr=1):
        return int(cache_rclient.incr(self.key, incr))


class ClassroomRate(object):
    """
    Rate of Classroom
    """

    CLASSROOM_RATE_C_PREFIX = 'CLMR_C'
    CLASSROOM_RATE_R_PREFIX = 'CLMR_R'

    def __init__(self, classroom_id):
        self.key_c = '%s:%s' % (self.CLASSROOM_RATE_C_PREFIX, classroom_id)
        self.key_r = '%s:%s' % (self.CLASSROOM_RATE_R_PREFIX, classroom_id)

    def get_rate(self):
        value = cache_rclient.get(self.key_r)
        count = cache_rclient.get(self.key_c)
        if value is None:
            return 0
        else:
            return int(value) / int(count)

    def get_count(self):
        value = cache_rclient.get(self.key_c)
        if value is None:
            return 0
        else:
            return int(value)

    def set_rate(self, value):
        cache_rclient.incr(self.key_r, value)
        self.increment()

    def delete_rate(self):
        cache_rclient.delete(self.key_r)

    rate = property(get_rate, set_rate, delete_rate)

    def increment(self, incr=1):
        return int(cache_rclient.incr(self.key_c, incr))

