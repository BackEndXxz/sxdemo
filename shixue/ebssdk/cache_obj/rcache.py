#coding=utf-


import time

import ebssdk.sdk_setting as settings


from ebssdk.cache_obj import cache_rclient


class VerifyCache(object):

    GET_PASSWORD_CACHE_PREFIX = 'GPVC'
    
    @classmethod
    def set_user_get_passwrod_verify_token(cls, verify_token, user_id, timeout=settings.CACHE_GET_PASS_DEFAULT_TIMEOUT): 
        cache_rclient.setex('%s:%s' % (cls.GET_PASSWORD_CACHE_PREFIX, verify_token), user_id, timeout)

    @classmethod
    def check_user_get_passwrod_verify_token(cls, verify_token): 
        return cache_rclient.get('%s:%s' % (cls.GET_PASSWORD_CACHE_PREFIX, verify_token))

class VideoStatsCache(object):

    _PREFIX = 'VSC'
    
    @classmethod
    def set_user_video_view_stats_token(cls, token, log_id, timeout=settings.CACHE_VIDEO_VIEW_STATS_DEFAULT_TIMEOUT): 
        cache_rclient.setex('%s:%s' % (cls._PREFIX, token), log_id, timeout)

    @classmethod
    def get_user_video_view_stats_log_id(cls, token): 
        return cache_rclient.get('%s:%s' % (cls._PREFIX, token))


class ClassroomOwnerLastJoin(object):

    _LAST_JOIN_PREFIX = 'COLJ'
    _GENERATE_PDF_PREFIX = 'GPDF'


    @classmethod
    def set_last_join(cls, user_id, classroom_id, timestamp=None):
        if not timestamp:
            timestamp = int(time.time())
        cache_rclient.set("%s:%s:%s" % (cls._LAST_JOIN_PREFIX, user_id, classroom_id), timestamp)
        cache_rclient.zadd("%s" % cls._GENERATE_PDF_PREFIX, classroom_id, timestamp)

    @classmethod
    def get_last_join(cls, user_id, classroom_id):
        return cache_rclient.get("%s:%s:%s" % (cls._LAST_JOIN_PREFIX, user_id, classroom_id))


class ClassroomPermission(object):

    _CLASSROOM_PERMISSION = 'CPSN'

    @classmethod
    def set_user_permission(cls, user_id, classroom_id, permission):
        cache_rclient.hset("%s:%s" % (cls._CLASSROOM_PERMISSION, classroom_id), user_id, permission)

    @classmethod
    def remove_user_permission(cls, user_id, classroom_id):
        cache_rclient.hdel("%s:%s" % (cls._CLASSROOM_PERMISSION, classroom_id), user_id)

    @classmethod
    def remove_users_permission(cls, user_ids, classroom_id):
        cache_rclient.hdel("%s:%s" % (cls._CLASSROOM_PERMISSION, classroom_id), *user_ids)

    @classmethod
    def get_user_permission(cls, user_id, classroom_id):
        return cache_rclient.hget("%s:%s" % (cls._CLASSROOM_PERMISSION, classroom_id), user_id)

    @classmethod
    def get_users_permissions(cls, user_ids, classroom_id):
        return cache_rclient.hmget("%s:%s" % (cls._CLASSROOM_PERMISSION, classroom_id), user_ids)


if __name__ == '__main__':
    #ps = ClassroomPermission.set_user_permission(2, 38, 2) 
    #ps = ClassroomPermission.set_user_persmission(7, 38, 2) 
    #ps = ClassroomPermission.get_users_permissions([1, 2, 3], 38)
    ps = ClassroomPermission.remove_users_permission([1, 2, 3], 38)
    ps = ClassroomPermission.get_users_permissions([1, 2, 7, 8], 38)
    print ps 
    ps = ClassroomPermission.get_users_permissions(['1', '2', '7', '8'], 38)
    print ps 

