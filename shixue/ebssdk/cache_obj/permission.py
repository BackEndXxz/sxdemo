#coding=utf-8

import simplejson

import utils.errors as err
import ebssdk.sdk_setting as settings

from functools import wraps
from flask import g, request

from ebssdk.cache_obj.rcache import ClassroomPermission  
from ebssdk import course as sdk_course 

from utils.view_tools import fail_jsonp, get_args, fail_json

class PERMISSION_STATUS:
    NONE = 0
    REQUESTING = 1
    LEVEL1 = 2

def set_user_permission(user_id, classroom_id, permission):
    ClassroomPermission.set_user_permission(user_id, classroom_id, permission)

def get_user_permission(user_id, classroom_id):
    permission = ClassroomPermission.get_user_permission(user_id, classroom_id)
    if not permission:
        return PERMISSION_STATUS.NONE
    return permission

def get_users_permissions(user_ids, classroom_id):
    permissions = ClassroomPermission.get_users_permissions(user_ids, classroom_id)
    return permissions

def delete_user_permission(user_id, classroom_id):
    return ClassroomPermission.remove_user_permission(user_id, classroom_id)

def delete_users_permission(user_ids, classroom_id):
    if type(user_ids) is not list:
        user_ids = user_ids.split(',')
    return ClassroomPermission.remove_users_permission(user_ids, classroom_id)


def ajax_query_permission_in_course_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        return func(*args, **kwargs)
    return decorated_view

def ajax_operate_permission_in_course_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        request.args = get_args(request)
        course_id = request.args.get('id', None)
        if not course_id:
            return fail_json(err.ERROR_CODE_INVALID_ARGS)
        course = sdk_course.core_get_course_by_id(course_id)
        if not course['ok']:
            return fail_json(err.ERROR_CODE_DATABASE_QUERY_ERROR)
        user = g.user
        if str(user.id) == str(course['data']['user_id']):
            return func(*args, **kwargs)
        permission = get_user_permission(user.id, course_id)
        if permission != PERMISSION_STATUS.LEVEL1:
            return fail_json(err.ERROR_CODE_NO_PERMISSION_INSERT_IN_CLASSROOM)         
        return func(*args, **kwargs)
    return decorated_view

def ajax_operate_permission_in_section_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        request.args = get_args(request)
        section_id = request.args.get('section_id', None)
        user_id = request.args.get('user_id', None)
        if not section_id:
            return fail_json(err.ERROR_CODE_INVALID_ARGS)
        section = sdk_course.core_get_section_by_id(section_id)
        if not section['ok']:
            return fail_json(err.ERROR_CODE_DATABASE_QUERY_ERROR, 'no section in permission check %s'%section_id)
        course = sdk_course.core_get_course_by_id(section['data']['course'])
        if not course['ok']:
            return fail_json(err.ERROR_CODE_DATABASE_QUERY_ERROR, 'no course in permission check %s'%section['data'])
        user = g.user
        if user:
            user_id = user.id
        if user.id == course['data']['user_id']:
            return func(*args, **kwargs)
        permission = get_user_permission(user.id, course['data']['id'])
        if permission != PERMISSION_STATUS.LEVEL1:
            return fail_json(err.ERROR_CODE_NO_PERMISSION_INSERT_IN_CLASSROOM)         
        return func(*args, **kwargs)
    return decorated_view


if __name__ == '__main__':
    p = get_users_permissions([1, 2, 7, 8], 38)
    print p
