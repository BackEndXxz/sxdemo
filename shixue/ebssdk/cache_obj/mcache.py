#coding=utf-8

import time
import memcache
import simplejson

import ebssdk.sdk_setting as settings


mc = memcache.Client(settings.MEMCACHED_SERVERS)


class CacheDownloadVerify(object):

    _prefix = "CDV"
    cache_client = mc

    @classmethod
    def make_cache_key(cls, token):
        key = '%s:%s'%(cls._prefix, token)
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        return key

    @classmethod
    def set_verify_key(cls, verify_type, verify_id, token, timeout=60*30):
        v = {"verify_type": verify_type, "verify_id": verify_id}
        cls.cache_client.set(cls.make_cache_key(token), simplejson.dumps(v), timeout)

    @classmethod
    def get_verify_key(cls, token):
        return cls.cache_client.get(cls.make_cache_key(token))

    @classmethod
    def check_verify_key(cls, verify_type, verify_id, token):
        doc = cls.cache_client.get(cls.make_cache_key(token))
        if doc:
            if str(doc.get("verify_type")) == str(verify_type) \
                    and str(doc.get("verify_id")) == str(verify_id):
                        return True
        return False

class ClassroomIntoSpam(object):

    _CLASSROOM_INTO_SPAM_KEY = 'CIS:%s-%s-%s'
    cache_client = mc

    @classmethod
    def _make_key(cls, user_id, classroom_id, device_type):
        return str(cls._CLASSROOM_INTO_SPAM_KEY %(user_id, classroom_id, device_type))
    
    @classmethod
    def check_spam(cls, user_id, classroom_id, device_type):
        """
        Check User get into classroom spam
        """
        if cls.cache_client.get(cls._make_key(user_id, classroom_id, device_type)):
            return True
        else:
            return False
    
    @classmethod
    def write_spam(cls, user_id, classroom_id, device_type, \
            timeout=settings.DEFAULT_CLASSROOM_INTO_SPAM_TIMEOUT):
        cls.cache_client.set(cls._make_key(user_id, classroom_id, device_type), int(time.time()), timeout)

