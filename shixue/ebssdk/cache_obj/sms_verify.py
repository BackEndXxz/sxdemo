#coding=utf-8


import time
import random

import ebssdk.sdk_setting as settings

from ebssdk.cache_obj import cache_rclient

class SmsRegVerifyCache(object):

    SMS_REG_CACHE_PREFIX = 'SMSRC:%s' % settings.APP_ID
    SMS_REG_SPAM_PREFIX = 'SMSRP:%s' % settings.APP_ID
    SMS_REG_TEXT_TEMPALTE = '您好，您在%s的账号验证码为%s，30分钟内有效，请勿外泄。' 

    @classmethod
    def set_sms_reg_verify_code(cls, code, mobile, timeout=settings.CACHE_SMS_REG_DEFAULT_TIMEOUT):
        cache_rclient.setex('%s:%s' % (cls.SMS_REG_CACHE_PREFIX, mobile), code, timeout)
        # set send msg spam cache
        cache_rclient.setex('%s:%s' % (cls.SMS_REG_SPAM_PREFIX, mobile), code, settings.CACHE_SMS_REG_SPAM_DEFAULT_TIMEOUT)

    @classmethod
    def check_is_spam_send_sms(cls, mobile):
        return cache_rclient.get('%s:%s' % (cls.SMS_REG_SPAM_PREFIX, mobile))

    @classmethod
    def get_sms_reg_verify_code(cls, mobile):
        return cache_rclient.get('%s:%s'% (cls.SMS_REG_CACHE_PREFIX, mobile))

    @classmethod
    def del_sms_reg_verify_code(cls, mobile):
        return cache_rclient.delete('%s:%s'% (cls.SMS_REG_CACHE_PREFIX, mobile))

    @classmethod
    def make_sms_text(cls, code):
        text = cls.SMS_REG_TEXT_TEMPALTE % (settings.APP_NAME, code)
        return text

    @classmethod
    def gen_sms_reg_code(cls):
        code = ''
        while len(code) < 6:
            code += str(random.randint(0, 9)) 
        return code



if __name__ == '__main__':
    code = SmsRegVerifyCache.gen_sms_reg_code()
    sms_text = SmsRegVerifyCache.make_sms_text(code)
    print sms_text



