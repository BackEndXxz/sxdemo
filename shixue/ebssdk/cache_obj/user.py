#coding=utf-8

from functools import wraps
import simplejson
from flask import session, g, redirect, request

from ebssdk import ebs_user
from ebssdk import file as sdk_file
from ebssdk import school as sdk_school
from ebssdk import ebs_session
import ebssdk.sdk_setting as settings
from ebssdk.cache_obj import cache_rclient
from ebssdk.cache_obj.cacheobject import CacheObject, CacheAdapter

import utils.errors as err
from utils.view_tools import fail_jsonp


class User(CacheObject):

    def __init__(self):
        CacheObject.__init__(self)
        self._adapter = CacheAdapter(cache_rclient, 'User')
        self._timeout = settings.DEFAULT_CACHE_USER_TIMEOUT

    def fetch_data(self, user_id):
        resp = ebs_user.core_get_user(user_id)
        if resp['ok']:
            user_dic = resp['data']
            if user_dic.get('avatar_file_id'):
                resp = sdk_file.core_get_file(user_dic.get('avatar_file_id'))
                if resp['ok']:
                    user_dic['avatar_uri'] =  resp['data']['location']
            if user_dic.get('university_id'):
                resp = sdk_school.core_get_school_by_id(user_dic.get('university_id'))
                if resp['ok']:
                    user_dic['university'] = resp['data']['title']

            self._fetch_data(user_dic)


    def is_active(self):
        return True

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def to_json(self):
        return simplejson.dumps(self.get_doc())


def get_user(user_id):
    assert user_id is not None
    user = User()
    user.fetch_cache(user_id)
    if user.is_dummy():
        user.fetch_data(user_id)
        if not user.is_dummy():
            user.save()
    return user

def delete_user_cache(user_id):
    user = get_user(user_id)
    user.delete()

def get_users(ids):
    cache_users = []
    cache_ids = []
    rest_ids = []

    user_keys = ['CO:User:%s'%_id for _id in ids ]
    if user_keys:
        cache_reguser_profiles = cache_rclient.mget(user_keys)
        #print 'cache_reguser_profiles:', cache_reguser_profiles 
    else:
        cache_reguser_profiles = []
    cache_profiles = cache_reguser_profiles

    cache_users_dic = {}
    for profile in cache_profiles:
        if profile:
            profile = simplejson.loads(profile)
            user = User()
            user.set_doc(profile)
            cache_users.append(user)
            cache_users_dic[int(user.id)] = user
            cache_ids.append(str(profile['id']))

    rest_ids = list(set(ids) - set(cache_ids))

    if not rest_ids:
        return cache_users
    else:
        resp = ebs_user.core_get_users(rest_ids)
        if resp['ok']:
            rest_user_data = resp['data']
            # Update Avatar Uri
            avatar_file_list = []
            for k, v in rest_user_data.iteritems():
                if v.get('avatar_file_id'):
                    avatar_file_list.append((k, v.get('avatar_file_id')))
            resp = sdk_file.core_get_files_by_ids([x[1] for x in avatar_file_list])
            if resp['ok']:
                file_info_dic = resp['data']
                for user_id, file_id in avatar_file_list:
                    file_info = file_info_dic[str(file_id)]
                    rest_user_data[user_id].update({'avatar_uri': file_info['location']})

            if rest_user_data:
                for k, v in rest_user_data.iteritems():
                    user = User()
                    user.set_doc(v)
                    user.save()
                    cache_users.append(user)
                    cache_users_dic[int(user.id)] = user
    ordered_users = []
    for id in ids:
        ordered_users.append(cache_users_dic.get(int(id)))
    return ordered_users


def core_user_auth(username, password):
    resp = ebs_user.core_user_auth(username, password)
    if resp['ok']:
        return resp['ok'], resp['data']['id']
    return resp['ok'], resp['reason']

def session_login_user(user, user_ip, site_id, force=False):
    if not force and not user.is_active():
        return False

    resp = ebs_session.core_create_session(user.id, user_ip, site_id)
    if resp['ok']:
        session_id = resp['data']['session_id']

        user_id = user.id
        session['user_id'] = user_id
        session['_fresh'] = True
        session['_id'] = session_id
        return True

    return False

def session_logout_user():
    if '_id' in session:
        session.pop('_id')

    if 'user_id' in session:
        session.pop('user_id')

    if '_fresh' in session:
        session.pop('_fresh')

    session.clear()
    return True

def redirect_login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        session_id = session.get('_id', None)
        if not session_id:
            return redirect('/?require_login=true&next=' + request.path)
        resp = ebs_session.core_query_session(session_id)
        if not resp['ok']:
            return redirect('/?require_login=true&next=' + request.path)
        g.user = get_user(resp['data']['user_id'])
        return func(*args, **kwargs)
    return decorated_view

def ajax_login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        session_id = session.get('_id', None)
        if not session_id:
            return fail_jsonp(err.ERROR_CODE_LOGIN_REQUIRED)
        resp = ebs_session.core_query_session(session_id)
        if not resp['ok']:
            return fail_jsonp(err.ERROR_CODE_LOGIN_REQUIRED)
        g.user = get_user(resp['data']['user_id'])
        return func(*args, **kwargs)
    return decorated_view

def check_user(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        session_id = session.get('_id', None)
        g.user = None
        if session_id:
            resp = ebs_session.core_query_session(session_id)
            if resp['ok']:
                g.user = get_user(resp['data']['user_id'])
        return func(*args, **kwargs)
    return decorated_view

def get_user_total_count():
    count = cache_rclient.get('TOTAL_USER_COUNT')
    if not count:
        resp = ebs_user.core_get_user_total_count()
        if resp['ok']:
            count = resp['data']
            cache_rclient.setex('TOTAL_USER_COUNT', count, settings.DEFAULT_CACHE_USER_TIMEOUT)
            return count
    return count


if __name__ == '__main__':
    """
    u = get_user(12)
    print u.get_doc()
    users = get_users([7,12])
    for u in users:
        print u.get_doc()
    """
    print get_user_total_count()


