#coding=utf-8
__author__='veggie'

import sdk_setting as settings
import requests_wrapper as rest

# Query
def core_get_video_by_id(id):
    return rest.get( '/core/api/video/query/info/', locals(), ('id',))

def core_get_videos_by_ids(ids):
    return rest.get('/core/api/video/query/by/ids/', locals(), ('ids',))

# Create
def core_create_video(mark, media_type, location_url='', media_val='', id=None):
    return rest.post('/core/api/video/create/', locals(),  ('mark', 'media_type', 'location_url', 'media_val', 'id'))

# Update
def core_update_video(id, mark=None, media_type=None, media_val=None):
    return rest.post('/core/api/video/update/', locals(), ('id', 'mark', 'media_type', 'media_val'))

# Delete
def core_batch_delete_video(ids):
    return rest.post('/core/api/video/batch/delete/', locals(), ('ids',))
