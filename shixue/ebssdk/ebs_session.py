#coding=utf-8
__author__='slide'

import requests_wrapper as rest

# Create
def core_create_session(user_id, user_ip, site_id):
    return rest.post('/core/api/session/create/', locals(), ('user_id', 'user_ip', 'site_id'))

def core_query_session(session_id):
    return rest.get('/core/api/session/query/', locals(), ('session_id',))


