#coding=utf-8
"""
Constants 
"""

class OauthSite:
    WEIBO = 1
    QQ = 2
    RENREN = 3
    DOUBAN = 4

class CoursePrivacyType:
    PUBLIC = 0
    PRIVATE = 1
