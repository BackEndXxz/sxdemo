#coding=utf-8
__author__='shshi'

import ebssdk.sdk_setting as settings 
import urlparse

def _get_absolute_url(relative_url, host=None):
    if not host:
        host = settings.HOST
    return urlparse.urljoin(host, relative_url)

def _debug_log(log):
    if settings.DEBUG:
        print log

def local_fail_json(err=0, description=''):
    return {'ok': False, 'reason': {'err': err, 'desc': description}}
