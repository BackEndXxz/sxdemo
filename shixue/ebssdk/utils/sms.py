#coding=utf-8

import requests

from ebssdk import sdk_setting as settings

def send_sms(mobile, text):
    if isinstance(mobile, unicode):
        mobile = mobile.encode("utf-8")
    if isinstance(text, unicode):
        text = text.encode("utf-8")
    absolute_url = "http://utf8.sms.webchinese.cn/?Uid=%s&Key=%s&smsMob=%s&smsText=%s" % (settings.SMS_USER_ID, settings.SMS_USER_KEY, mobile, text)
    req = requests.get(absolute_url)
    if req.status_code == 200:
        return True, req.content
    return False, req.content


if __name__ == '__main__':
    print send_sms('1359027938', '我是这个邀请码:994313')


