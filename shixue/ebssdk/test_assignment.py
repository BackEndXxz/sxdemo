#coding=utf-8
__author__='shshi'

import time
from assignment import *

if __name__ == "__main__":
    start_time = int(time.time())
    end_time = start_time + 100000 
    c = core_create_assignment(3, 1, 'test 2', end_time) 
    c = core_get_assignment_by_id(3)
    c = core_get_assignment_id_list_by_course(3)
    c = core_insert_submission(1, 1, 1, 'test assignment') 
    c = core_get_submission_id_list_by_assignment(1)
    c = core_get_submission_id_list_by_user(1)
