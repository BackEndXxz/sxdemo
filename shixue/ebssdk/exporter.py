#coding=utf-8
__author__='slide'

import requests_wrapper as rest

import sdk_setting as settings

# Query
def core_get_frame_pdf(frame_id):
    return rest.get('/exporter/api/download/pdf/frame/%s/' % frame_id, locals(), (), host=settings.PDF_EXPORTER_HOST, format='raw')


def core_get_section_pdf(section_id):
    return rest.get('/exporter/api/download/pdf/section/%s/' % section_id, locals(), (), host=settings.PDF_EXPORTER_HOST, format='raw')


if __name__ == '__main__':
    core_get_frame_pdf(100)
