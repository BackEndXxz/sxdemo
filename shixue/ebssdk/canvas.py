#coding=utf-8
__author__='veggie'

import sdk_setting as settings
import requests_wrapper as rest

# Query
def core_get_canvas_by_frame(frame_id):
    return rest.get('/core/api/canvas/get_by_frame_id/', locals(), ('frame_id',))

def core_get_canvas_background_images_log_by_frame(frame_id):
    return rest.get('/core/api/canvas/get_background_images_log_by_frame_id/', locals(), ('frame_id', ))

def core_get_canvas_note_by_frame_and_user(frame_id, user_id):
    return rest.get('/core/api/canvas/note/get_by_frame_id_and_user_id/', locals(), ('frame_id', 'user_id'))

def core_get_canvas_message_ids_by_classroom(classroom_id):
    return rest.get('/core/api/canvas/message/query/ids/by/classroom/', locals(), ('classroom_id', ))

def core_get_canvas_messages_by_ids(ids):
    ids_str = ids
    if type(ids_str) is not str:
        ids_str = ','.join(ids)
    ids = ids_str
    return rest.get('/core/api/canvas/messages/', locals(), ('ids', ))

def core_get_assignment_marks(assignment_id, submission_page_id):
    return rest.get('/core/api/canvas/assignment_mark/get_by_assignment_and_page_id_api/', locals(), ('assignment_id', 'submission_page_id'))


# Create
def core_insert_canvas_object(frame_id, canvas_object):
    return rest.post('/core/api/canvas/insert_object/', locals(), ('frame_id', 'canvas_object'))

def core_insert_message_object(message_object):
    return rest.post('/core/api/canvas/message/insert/', locals(), ('message_object', ))

def core_insert_canvas_note(frame_id, user_id, note_object):
    return rest.post( '/core/api/canvas/note/insert_object/', locals(), ('frame_id', 'user_id', 'note_object'))

def core_insert_assignment_mark(assignment_id, canvas_object):
    return rest.post('/core/api/canvas/assignment_mark/insert_object/', locals(), ('assignment_id', 'canvas_object'))

# Update
def core_update_canvas(frame_id, object_id, canvas_object):
    return rest.post( '/core/api/canvas/update_object/', locals(),  ('frame_id', 'object_id', 'canvas_object'))

def core_update_canvas_note(frame_id, object_id, canvas_object):
    return rest.post('/core/api/canvas/note/update_object/', locals(),  ('frame_id', 'object_id', 'canvas_object'))

def core_update_assignment_mark(assignment_id, object_id, canvas_object):
    return rest.post('/core/api/canvas/assignment_mark/update_object/', locals(),  ('assignment_id', 'object_id', 'canvas_object'))

# Delete
def core_delete_canvas_objects(frame_id, object_ids, replay_id=None, ms=None):
    return rest.post('/core/api/canvas/delete_objects/', locals(), ('frame_id', 'object_ids', 'replay_id', 'ms'))

def core_delete_canvas_notes(user_id, frame_id, object_ids):
    return rest.post('/core/api/canvas/note/delete_objects/', locals(), ('user_id', 'frame_id', 'object_ids'))

def core_delete_assignment_marks(assignment_id, object_ids):
    return rest.post('/core/api/canvas/assignment_mark/delete_objects/', locals(), ('assignment_id', 'object_ids'))


# Replay Traces
def core_get_frames_traces(frame_ids):
    ids_str = frame_ids
    if type(ids_str) is not str:
        ids_str = ','.join(frame_ids)
    frame_ids = ids_str
    return rest.get('/core/api/canvas/frames/traces/', locals(), ('frame_ids', ))

def core_get_replay_actions(replay_id):
    return rest.get('/core/api/canvas/replay/action/%s/'%replay_id, locals(), ())

def core_insert_action_list(action_list):
    return rest.post('/core/api/canvas/record/action_list/insert/', locals(), ('action_list', ))

def core_frame_change_action(replay_id, frame_id, user_id, ms):
    return rest.post('/core/api/canvas/frame/change/', locals(), ('replay_id', 'frame_id', 'user_id', 'ms'))

def core_add_cursor_list(replay_id, frame_id, user_id, ms, cursor_list):
    return rest.post('/core/api/canvas/frame/change/', locals(), ('replay_id', 'frame_id', 'user_id', 'ms'))
