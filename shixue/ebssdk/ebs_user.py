#coding=utf-8
__author__='slide'

import requests_wrapper as rest

# Query
def core_user_auth(username, password):
    return rest.get('/core/api/user/query/auth/', locals(), ('username', 'password'))

def core_get_user(user_id):
    return rest.get('/core/api/user/query/info/', locals(), ('user_id', ))

def core_get_users(ids):
    if isinstance(ids, list):
        ids = ','.join([str(id) for id in ids])
    return rest.get('/core/api/user/query/batch/', locals(), ('ids', ))

def core_get_user_avatar(user_id):
    return rest.get('/core/api/user/query/avatar/', locals(), ('user_id', ))

def core_get_user_file(id):
    return rest.get('/core/api/user/query/file/', locals(), ('id', ))

def core_get_user_apply(user_id):
    return rest.get('/core/api/user/query/apply/', locals(), ('user_id', ))

def core_list_user_file(user_id):
    return rest.get('/core/api/user/query/file/list/by/user_id/', locals(), ('user_id', ))

def core_get_user_file_id_list(user_id):
    return rest.get('/core/api/user/query/id/file/list/by/user_id/', locals(), ('user_id', ))

def core_get_user_files_by_ids(ids):
    if isinstance(ids, list):
        ids = ','.join([str(id) for id in ids])
    return rest.get('/core/api/user/query/files/by/ids/', locals(), ('ids', ))

def core_get_user_video_id_list(user_id):
    return rest.get('/core/api/user/query/id/video/list/by/user_id/', locals(), ('user_id', ))

def core_get_user_videos_by_ids(ids):
    if isinstance(ids, list):
        ids = ','.join([str(id) for id in ids])
    return rest.get('/core/api/user/query/videos/by/ids/', locals(), ('ids', ))

def core_get_user_total_count():
    return rest.get('/core/api/user/query/user/total/count/', locals(), ('', ))

# Create
def core_create_user(username, password, email, id=None):
    url = '/core/api/user/create/'
    return rest.post(url, locals(), ('username', 'password', 'email', 'id'))

def core_create_user_file(user_id, file_id, custom_name):
    url = '/core/api/user/create/file/'
    return rest.get(url, locals(), ('user_id', 'file_id', 'custom_name'))

def core_create_user_video(user_id, video_id, custom_name):
    url = '/core/api/user/create/video/'
    return rest.get(url, locals(), ('user_id', 'video', 'custom_name'))

def core_create_user_apply(user_id, status=None):
    url = '/core/api/user/create/apply/'
    return rest.get(url, locals(), ('user_id', 'status'))

# Update
def core_update_user_password(id, old_password, new_password):
    url = '/core/api/user/update/password/'
    return rest.post(url, locals(), ('id', 'old_password', 'new_password'))

def core_update_user_avatar(user_id, file_id):
    url = '/core/api/user/update/avatar/'
    return rest.get(url, locals(), ('user_id', 'file_id'))

def core_update_user_role(user_id, role):
    url = '/core/api/user/update/role/'
    return rest.get(url, locals(), ('user_id', 'role'))

def core_update_user_apply(user_id, status=None):
    url = '/core/api/user/update/apply/'
    return rest.get(url, locals(), ('user_id', 'status'))

def core_update_user_file(id, file_id=None, custom_name=None):
    url = '/core/api/user/update/file/'
    return rest.get(url, locals(), ('id', 'file_id', 'custom_name'))

def core_update_user_video(id, custom_name):
    url = '/core/api/user/update/video/'
    return rest.get(url, locals(), ('id', 'custom_name'))

def core_verify_user_email(user_id, verify_token):
    url = '/core/api/user/update/email/verified/'
    return rest.post(url, locals(), ('user_id', 'verify_token'))

def core_update_user_profile(user_id, email=None, nick=None, university_id=None, job=None, \
        degree=None, major=None, mobile=None, country=None, city=None, introduction=None):
    url = '/core/api/user/update/info/'
    return rest.get(url, locals(), ('user_id', 'email', 'nick', 'university_id', 'job', 'degree',\
            'major', 'mobile', 'country', 'city', 'introduction'))

# Delete
def core_delete_user_file(id):
    url = '/core/api/user/delete/file/'
    return rest.delete(url, id)

def core_delete_user_video(id):
    url = '/core/api/user/delete/video/'
    return rest.delete(url, id)


def core_update_user_file(id, file_id=None, custom_name=None):
    url = '/core/api/user/update/file/'
    return rest.get(url, locals(), ('id', 'file_id', 'custom_name'))

def core_update_user_video(id, custom_name):
    url = '/core/api/user/update/video/'
    return rest.get(url, locals(), ('id', 'custom_name'))

def core_verify_user_email(user_id, verify_token):
    url = '/core/api/user/update/email/verified/'
    return rest.post(url, locals(), ('user_id', 'verify_token'))

def core_update_user_profile(user_id, email=None, nick=None, university_id=None, job=None, degree=None, major=None, user_type=None):
    url = '/core/api/user/update/info/'
    return rest.get(url, locals(), ('user_id', 'email', 'nick', 'university_id', 'job', 'degree', 'major', 'user_type'))

# Delete
def core_delete_user_file(id):
    url = '/core/api/user/delete/file/'
    return rest.delete(url, id)

def core_delete_user_video(id):
    url = '/core/api/user/delete/video/'
    return rest.delete(url, id)


