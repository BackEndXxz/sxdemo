#coding=utf-8
__author__='shshi'
import time
from course import *

def test_course_basic():
    c = core_get_course_by_id(1)
    c = core_get_course_addition_by_id(1)
    c = core_get_arrangement_by_id(4)
    l = core_get_arrangement_ids(1)
    c = core_get_arrangements_by_ids(['7', '6', '3'])

    c = core_get_section_by_id(1)
    l = core_get_section_ids(1)
    c = core_get_sections_by_ids(['1', '4', '3'])
    
    c = core_get_frame_by_id(1)
    l = core_get_frame_ids(1)
    c = core_get_frames_by_ids(['3', '2', '1'])

    core_get_cooperation_school_ids(1)
    
    core_get_discipline_by_id(1)
    core_get_discipline_category_ids(1)
    core_get_disciplines_by_ids(['1', '2'])

    core_get_category_by_id(1)
    core_get_categorys_by_ids(['1', '2'])
    core_get_categorys_by_discipline_id(1)
    

def test_create():
    c = core_create_course(1, 'hello', 'heillo')
    c = core_insert_section(c['data']['id'], title='hello')
    c = core_insert_frame(c['data']['id'], title='hello')

def test_update():
    core_update_course(38, is_private=False)
    c = core_get_course_by_id(38)


def test_delete_api():
    core_delete_course(3)
    core_delete_course_arrangement(2)
    core_delete_course_cooperation(2)
    core_delete_course_reference(2)
    core_delete_course_discipline_category(2)
    core_delete_section(2)
    core_delete_frame(2)
    
def test_course_addition():
    start_time = time.time()
    end_time = start_time + 100000 
    #c = core_insert_section(38, 'section one')
    #c = core_create_course(7, 'shshi hello', 'heillo')
    #course_id = c['data']['id']
    #c = core_insert_user_learning_course(7, course_id)
    #c = core_insert_user_learning_course(7, course_id)
    #c = core_insert_user_marked_course(7, course_id)
    c = core_insert_frame(section_id=33, title='first frame', introduction=None, pre_frame_id='56')
#    c = core_insert_section_video(7, 1, 'video:%s'%start_time)
   # c = core_insert_course_arrangement(2, 'first section', start_time, end_time, pre_arrangement_id=9)

if __name__ == "__main__":
    #test_course_addition()
    #test_update()
    test_create()
