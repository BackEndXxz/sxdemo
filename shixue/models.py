#coding: utf-8 -*-
import datetime
from sqlalchemy import create_engine
from sqlalchemy import Column,DateTime
from sqlalchemy.types import String, Integer, CHAR, BIGINT,DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Integer, ForeignKey, String, \
        Column, UniqueConstraint, ForeignKeyConstraint
from sqlalchemy.orm import relationship

#engine = create_engine('mysql://root:root@localhost:3306/course?charset=utf8',encoding='utf-8',convert_unicode=True, echo=True)
engine = create_engine('sqlite:///course.db',encoding='utf-8',convert_unicode=True, echo=True)

Base = declarative_base()

class Course(Base):
    __tablename__ = 'course'
    id = Column(Integer, primary_key = True)
    course_title = Column(String(128))
    course_img = Column(String(128))
    course_teacher = Column(String(32))
    course_info = Column(String(1024))
    course_price = Column(String(32))
    upload_time = Column(DateTime, default=datetime.datetime.utcnow)
    course_length = Column(String(64))
    play_times = Column(Integer)
    evaluate = Column(String(32))
    video_url = Column(String(512))
    video_quality = Column(String(32))
    homework_title = Column(String(64))
    homework_url = Column(String(128))
    the_num_of_eva = Column(Integer)
    the_num_of_stu = Column(Integer)

    def to_json(self):
        return {
            "id":self.id,
            "course_title":self.course_title,
            "course_techer":self.course_teacher,
            "course_info":self.course_info,
            "course_img":self.course_img,

        }


if __name__ == '__main__':
    Base.metadata.create_all(engine)

