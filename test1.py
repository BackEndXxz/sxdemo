#encoding: utf-8
import json
import hashlib

from flask import Flask, request, jsonify, Response, render_template
from flask_login import (current_user, LoginManager,
                             login_user, logout_user,
                             login_required)

from werkzeug import secure_filename

from sqlalchemy import create_engine, ForeignKey, Column, Integer, String, Text, DateTime,\
    and_, or_, SmallInteger, Float, DECIMAL, desc, asc, Table, join, event
from sqlalchemy.orm import relationship, backref, sessionmaker, scoped_session, aliased, mapper
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.sql import select

import flask_admin as admin
from flask_admin.contrib import fileadmin

#from flask.ext.superadmin import Admin, model


import os
import os.path as op

engine = create_engine(
	"postgresql+psycopg2://flask:ubuntu@localhost/mydb",
    isolation_level="READ UNCOMMITTED")

Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session() 

app = Flask(__name__)

#admin = Admin(app)
#admin.add_view(ModelView(users, db.session))

app.secret_key = 'youdontknowme'


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


def ok_json(data=''):
    return jsonify(ok=True, data=data)

def fail_json(reason='', http_code=200):
    if http_code==200:
        return jsonify(ok=False, reason=reason)
    else:
        response = Response()
        response.status_code = http_code
        response.data = reason
        return response


def ok_jsonp(data={}, callback_name='callback'):
    jsonp_callback =  request.args.get(callback_name, None)
    if jsonp_callback:
        return Response(
                "%s(%s);" % (jsonp_callback, json.dumps({'ok': True, 'data':data})),
                mimetype="text/javascript"
                )
    return ok_json(data)

def fail_jsonp(reason='', callback_name='callback'):
    jsonp_callback =  request.args.get(callback_name, None)
    if jsonp_callback:
        return Response(
                "%s(%s);" % (jsonp_callback, json.dumps({'ok': False, 'reason':reason})),
                mimetype="text/javascript"
                )
    return fail_json(reason)



def get_args(request, *descriptions):
    args = request.get_json() if request.method == 'POST' else request.args
    if not args and request.data and request.method == 'POST':
        args = json.loads(request.data)
    if not args:
        args = request.form
    return args


#注册
@app.route('/api/register',methods=['GET','POST'])
def register():
    args = get_args(request)
    nickname = args.get('nickname')
    email = args.get('email')
    password = args.get('password')

    content = User(nickname=nickname,email=email, password=password)

    session.add(content)
    try:
        session.commit()
    except:
        session.rollback()
        raise
    return ok_jsonp({'nickname':nickname,'email': email, 'password': password})

#登录
@login_manager.user_loader
def load_user(user_id):
    return User.objects(id=user_id).first()


@app.route('/api/login', methods=['GET','POST'])
def login():
    args = get_args(request)
#    user_id = args.get('user_id')
    email = args.get('email')
    password = args.get('password')
    user = session.query(User).filter_by(email=email).first()
    userpassword_hash = hashlib.sha1(password).hexdigest()
   

    if not user:
        return fail_jsonp({ "reason": "User is not exist"})
   
    if user.password != userpassword_hash:
        return fail_jsonp({ "reason": "Password error"})   
    else:
        return ok_jsonp({"email":email,"user_id":user.user_id})


#登出
@app.route('/logout', methods=['POST'])
def logout():
    logout_user()
    return jsonify(**{'result': 200,
                      'data': {'message': 'logout success'}})

      

 #查询基本信息
@app.route('/api/query/info',methods=['GET','POST'])
def query_info():
    args = get_args(request)
    user_id = args.get('user_id')
   # email = args.get("email")
    user = session.query(User).filter_by(user_id=user_id).first()
    print user
    if user:
#        table = User
#	conn = engine.connect()
#        sql = select([table],table.user_id==user_id)
#        a = conn.execute(sql).fetchone()
#    conn.close()
#    b = json.dumps(a)
#    return ok_jsonp(b)
        return ok_jsonp({"user_id":user_id,"nickname":user.nickname,"real_name":user.real_name,"email":user.email})


#更新基本信息
@app.route('/api/info/update', methods=['GET','POST'])
def update_message():
    args = get_args(request)
    user_id = args.get('user_id') 
    nickname = args.get('nickname')
    real_name = args.get('real_name')
    sex = args.get('sex')
    mobel = args.get('mobel')
    qq = args.get('qq')
    adress = args.get('adress')
    post_code = args.get('post_code')
    role =  args.get('role')
    content = User(qq=qq)

    user = session.query(User).filter_by(user_id=user_id).first()


    if user is not None:
        # myadd
        session.query(User).filter_by(user_id=user_id).update({"nickname":  nickname})

        #session.update(content)
        try:
            session.commit()
        except:
            session.rollback()
            
        return ok_jsonp({'qq':user.qq})


#    connection = engine.connect()
 #   user_table = Useri
#    update_sql = User.update()\
#                              .where(User.user_id==user_id)\
#                              .values(content=content)
#    try:
#        connection.execute(update_sql)
#    except:
#        import traceback;traceback.print_exc()
#        connection.close()
#        return fail_jsonp('errors.ERROR_DB_ERROR')
#    connection.close()

#    return ok_jsonp()






class User(Base):
    __tablename__ = 'user'
    user_id = Column(Integer, primary_key = True)
    nickname = Column(String(32), index = True)
    real_name = Column(String(32))
    sex = Column(String(4))
    mobel =Column(String(32))
    qq =Column(String(32))
    adress = Column(String(128))
    post_code = Column(String(32))
    email = Column(String(64))
    password = Column(String(128))
    role = Column(String(12))


class video_storage(Base):
    __tablename__ = 'file_uploaded'
    id = Column(Integer, primary_key = True)
    file = Column(String(512), index = True)
    title = Column(String(32))
    info = Column(String(1024))


# yet another file upload html form

# file max 512MB
app.config['MAX_CONTENT_LENGTH'] = 512 * 1024 * 1024


# 设置允许上传的文件类型
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg',  'mp4'])

def allowed_file(filename):
    # 判断文件的扩展名是否在配置项ALLOWED_EXTENSIONS中
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS



# 设置上传文件存放的目录
UPLOAD_FOLDER = './files'
 
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # 获取上传过来的文件对象
        file = request.files['file']
        title = request.form.get('title','default value')
        id = request.form.get('id','default value')
        info = request.form.get('info','null')

        # 检查文件对象是否存在，且文件名合法
        if file and allowed_file(file.filename):
            # 去除文件名中不合法的内容
            filename = secure_filename(file.filename)
            # 将文件保存在本地UPLOAD_FOLDER目录下
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            
            video_upload = video_storage(file=filename, title=title, id=id, info=info)          
            
            session.add(video_upload)

            try:
                session.commit()
            except:
                session.rollback()

            return 'Done !!!'


        else:    # 文件不合法
            return 'Upload Failed'
    else:    # GET方法
        return render_template('upload.html')





if __name__ == '__main__':    

    Base.metadata.create_all(engine)

    app.run(debug=True, host='127.0.0.1', port=8080)

